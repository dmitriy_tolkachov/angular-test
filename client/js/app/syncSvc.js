/**
 * Created by Dima on 01.08.2015.
 */
'use strict';
angular.module('syncSvc',[]).factory('syncSvc',function($http, $q){
    /**
     * load all contacts
     * @returns {*} promise
     */
    //TODO: load by pages
    var loadContacts = function(){
        var d = $q.defer();
        $http.get(AppSettings.apiUrl+'contacts').then(function(response){
            //ok
            _.each(response.data,function(item){
                var c = new Contact(item.firstName,item.phone,item.lastName,item.email,item._id);
                ContactsCtrl.addContact(c);
            })
            d.resolve(response);

        },function(response){
            //fail
            d.reject(response);
        })
        return d.promise;
    }

    /**
     * save new contact on server
     * @param {Contact} contact
     * @returns {*} promise
     */
    var addContact = function(contact){
        var d = $q.defer();

        $http.post(AppSettings.apiUrl+'contacts',contact).then(function(response){
            //ok
            d.resolve(response);
        },function(response){
            //fail
            d.reject(response);
        });

        return d.promise;
    }

    /**
     * update existing contact on server
     * @param {Contact} contact
     * @returns {*} promise
     */
    var updateContact = function(contact){
        var d = $q.defer();

        $http.put(AppSettings.apiUrl+'contacts',contact).then(function(response){
            //ok
            d.resolve(response);
        },function(response){
            //fail
            d.reject(response);
        });

        return d.promise;
    }

    /**
     * delete contact by id
     * @param {string} id - contact id
     * @returns {*} promise
     */
    var deleteContact = function(id){
        var d = $q.defer();

        if(!id){
            d.reject(new Error('Contact id is not specified.'));
        }

        $http.delete(AppSettings.apiUrl+'contacts/'+id).then(function(response){
            //ok
            d.resolve(response);
        },function(response){
            //fail
            d.reject(response);
        })
        return d.promise;
    }

    return {
        loadContacts:loadContacts,
        addContact:addContact,
        deleteContact:deleteContact,
        updateContact:updateContact
    };
})

/*
*/
