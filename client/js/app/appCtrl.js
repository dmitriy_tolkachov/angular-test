'use strict';
angular.module('appCtrl', ['syncSvc'])
    .controller('appCtrl', function ($scope,syncSvc) {
        //properties

        $scope.apiUrl = angular.copy(AppSettings.apiUrl);
        //all contacts
        $scope.contacts = [];
        //active contact
        $scope.contact = null;
        $scope.notifications = null;

        //methods

        $scope.onSettingsCancel = function(){
            $scope.apiUrl = angular.copy(AppSettings.apiUrl);
        }
        $scope.onSettingsSubmit = function(){
            AppSettings.apiUrl = angular.copy($scope.apiUrl);
        }

        $scope.init = function(){
            syncSvc.loadContacts().then(function(){
                $scope.contacts = ContactsCtrl.getContacts();

                if(!$scope.contacts || !$scope.contacts.length){
                    alert('There are no contacts yet. Please create first one.')
                }
            },function(){
                alert('Can`t load contacts.')
            })
        }
        /**
         * create new empty contact on client
         */
        $scope.onCreate = function(){
            $scope.contact = new Contact();
        }
        $scope.onEdit = function(contact){
            $scope.contact = angular.copy(contact);
        }
        /**
         * save contact (on server and client)
         */
        $scope.onSubmit = function(){
            $scope.notification = null;
            //validate contact before submit
            var validationRes = $scope.contact.isValid();
            if(validationRes.isValid){
                //save on server
                if($scope.contact._id){
                    //contact exists - update
                    syncSvc.updateContact($scope.contact).then(function(){
                        //ok
                        ContactsCtrl.addContact($scope.contact);
                        $scope.onCancel();
                    },function(){
                        //fail
                        alert('Can`t update contact on server.')
                    })
                }
                else{
                    //new contact - save
                    syncSvc.addContact($scope.contact).then(function(){
                        //save on client
                        ContactsCtrl.addContact($scope.contact);
                        $scope.onCancel();
                    },function(){
                        alert('Can`t save contact on server.')
                    })
                }
            }
            else{
                $scope.notifications = Utils.getListOfErrors(validationRes);
            }
        }
        $scope.onDelete = function(){
            //delete on server
            syncSvc.deleteContact($scope.contact._id).then(function(){
                //ok
                //delete on client
                ContactsCtrl.deleteById($scope.contact._id);
                $scope.onCancel();
            },function(){
                //fail
                alert('Can`t delete this contact.')
            })
        }
        $scope.onCancel = function(){
            $scope.contact = null;
            $scope.notifications = null;
        }

        //init
        $scope.init();
    }
);