'use strict';
/**
 * contact manager
 */
var ContactsCtrl = (function(){
    //private
    var _contactsList = [];

    var _getContactIndexById = function(id){
        return _.findIndex(_contactsList,{_id:id});
    }

    /**
     * delete contact by id
     * @param id
     */
    var deleteById = function(id){
        var index = _getContactIndexById(id);
        if(index!=-1){
            _contactsList.splice(index,1);
        }
        else{
            return new Error('no contact with id ' + id);
        }
    }
    /**
     * add or replace (if such exists) contact to the storage
     * @param {Contact} contact
     */
    var addContact = function(contact){
        if(!contact){
            return new Error('no data to add to the list of contacts');
        }

        //set identifier for the new contact
        if(!contact._id){
            //new contact
            contact._id = Utils.guid();
            _contactsList.push(contact);
        }
        else{
            //existing contact
            var index = _getContactIndexById(contact._id);
            if(index!=-1){
                //such item exists - replace it
                _contactsList.splice(index,1,contact);
            }
            else{
                //no such item - add it
                _contactsList.push(contact);
            }
        }
    }

    /**
     * get list of existing contacts
     * @returns {Array}
     */
    var getContacts = function(){
        //TODO: think about returning copy of the list (in this case will need to get contacts after submit)
        return _contactsList;
    }
    /**
     * get contact by its id
     * @param id
     * @returns {*}
     */
    var getById = function(id){
        if(id){
            return _.find(_contactsList,{_id:id});
        }
        else{
            return new Error('id is not defined.');
        }
    }

    //public
    return{
        getById:getById,
        getContacts:getContacts,
        addContact:addContact,
        deleteById:deleteById
    }
}());