'use strict';
var Utils = (function(){
    //private

    /**
     * generate unique identifier
     * @returns {string}
     */
    var guid = function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
    /**
     * check if input is valid email address
     * @param email
     * @returns {boolean}
     */
    var validateEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    /**
     * check if input is a number
     * @param n
     * @returns {boolean}
     */
    var isNumber = function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    /**
     * compose list of error messages from validation result object
     * @param {Validation} validationRes
     * @returns {Array}
     */
    var getListOfErrors = function(validationRes){
        var notifications = [];
        if(validationRes.details && validationRes.details.length){
            _.each(validationRes.details,function(item){
                notifications.push(item.msg);
            })
        }
        else{
            notifications = [validationRes.msg?validationRes.msg:'can`t define errors in validation result'];
        }
        return notifications;
    }

    //public
    return{
        guid:guid,
        validateEmail:validateEmail,
        isNumber:isNumber,
        getListOfErrors:getListOfErrors
    }
}())