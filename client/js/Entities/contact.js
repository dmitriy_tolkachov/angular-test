'use strict';
/**
 * contact class
 * @param fName
 * @param phone
 * @param lName
 * @param email
 * @constructor
 */
var Contact = function(fName,phone,lName,email,id){
    this._id = id?id:null;

    this.firstName = fName;
    this.lastName = lName;
    this.email = email;
    this.phone = phone;

    //TODO: think about - this functions can be placed in ContactCtrl
    this.isValid = function(){
        var res = new Validation(true,'contact');

        /*if(!this._id){
            res.isValid = false;
            res.details.push(new Validation(false,'id','contact has no identifier'));
        }*/

        //name mandatory, <=32
        if(!this.firstName){
            res.isValid = false;
            res.details.push(new Validation(false,'first name','first name is required'));
        }
        if(this.firstName && this.firstName.length>AppSettings.firstNameMaxLength){
            res.isValid = false;
            res.details.push(new Validation(false,'first name','first name should be up to '+AppSettings.firstNameMaxLength+' characters'));
        }
        //phone mandatory, <=14, digits
        if(!this.phone){
            res.isValid = false;
            res.details.push(new Validation(false,'phone','phone is required'));
        }
        if(this.phone && this.phone.toString().length>AppSettings.phoneMaxLength){
            res.isValid = false;
            res.details.push(new Validation(false,'phone','phone should be up to '+AppSettings.phoneMaxLength+' digits'));
        }
        if(this.phone && !Utils.isNumber(this.phone)){
            res.isValid = false;
            res.details.push(new Validation(false,'phone','phone should contain only digits'));
        }
        //last name <=32
        if(this.lastName && this.lastName.length>AppSettings.lastNameMaxLength){
            res.isValid = false;
            res.details.push(new Validation(false,'last name','last name should be up to '+AppSettings.lastNameMaxLength+' characters'));
        }
        //email <=32
        if(this.email){
            //email exists: check if email, check length
            if(this.email>AppSettings.emailMaxLength){
                res.isValid = false;
                res.details.push(new Validation(false,'email','email should be up to '+AppSettings.emailMaxLength+' characters'));
            }

            if(!Utils.validateEmail(this.email)){
                res.isValid = false;
                res.details.push(new Validation(false,'email','email is not in correct format'));
            }
        }

        return res;
    }
    this.getFullName = function(){
        var fullName = this.firstName;
        return this.lastName? fullName + ' ' + this.lastName : fullName;
    }
}