'use strict';
/**
 * validation class
 * @param isValid
 * @param propName
 * @param msg
 * @constructor
 */
var Validation = function(isValid,propName,msg){
    this.isValid = isValid;
    this.propName = propName;
    this.msg = msg;
    //array of Validation
    this.details=[];
}