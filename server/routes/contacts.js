var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  var db = req.db;
  var collection = db.get('contacts');
  collection.find({},{},function(e,docs){
    res.json(docs);
  });
});

/* POST save contact. */
router.post('/',function(req,res,next){
    var db = req.db;
    var collection = db.get('contacts');

    console.log('--- SAVE CONTACT %o',req.body)
    //TODO: convert to contact object, validate on server

    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
})

router.put('/',function(req,res,next){
    var db = req.db;
    var collection = db.get('contacts');
    var contactToUpdate = req.body._id;

    console.log('--- UPDATE CONTACT %o',contactToUpdate);

    collection.findAndModify({_id:contactToUpdate}, req.body, function(err) {
        res.send((err === null) ? { msg: '' } : { msg:'error: ' + err });
    });
})

router.delete('/:id',function(req,res,next){
    var db = req.db;
    var collection = db.get('contacts');
    var contactToDelete = req.params.id;

    console.log('--- DELETE CONTACT %o',contactToDelete);

    collection.remove({ '_id' : contactToDelete }, function(err) {
        res.send((err === null) ? { msg: '' } : { msg:'error: ' + err });
    });
})

module.exports = router;
